import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputTextComponent } from './components/input-text/input-text.component';
import { FormSpinnerComponent } from './components/form-spinner/form-spinner.component';
import { DescPipe } from './pipes/desc/desc.pipe';



@NgModule({
  declarations: [
    InputTextComponent,
    FormSpinnerComponent,
    DescPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
