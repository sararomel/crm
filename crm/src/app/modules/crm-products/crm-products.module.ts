import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrmProductsRoutingModule } from './crm-products-routing.module';
import { CrmProductsComponent } from './pages/crm-products/crm-products.component';
import { ItemComponent } from './pages/item/item.component';


@NgModule({
  declarations: [
    CrmProductsComponent,
    ItemComponent
  ],
  imports: [
    CommonModule,
    CrmProductsRoutingModule
  ]
})
export class CrmProductsModule { }
