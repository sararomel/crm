import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';
import { FooterComponent } from './components/main-layout/footer/footer.component';
import { HeaderComponent } from './components/main-layout/header/header.component';
import { LayoutComponent } from './components/main-layout/layout/layout.component';



@NgModule({
  declarations: [
    LoaderComponent,
    FooterComponent,
    HeaderComponent,
    LayoutComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CoreModule { }
